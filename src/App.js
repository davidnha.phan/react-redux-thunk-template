import React from "react";
import { BrowserRouter, Routes } from "react-router-dom";

import { listRoute } from "./utils/routes";
import { routes } from "./routes";

function App() {
  return (
    <BrowserRouter>
        <Routes>{listRoute(routes)}</Routes>
    </BrowserRouter>
  );
}

export default App;
