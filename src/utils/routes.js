import React from "react";
import { Route } from "react-router-dom";

export const listRoute = (list) => {
  return list.map((route, index) => {
    return (
      <Route
        key={index}
        path={route.path}
        element={
          <React.Suspense fallback={<>...</>}>
            <route.component />
          </React.Suspense>
        }
      />
    );
  });
}


