import React from "react";

export const routes = [
  {
    path: "/",
    component: React.lazy(() => import("../containers/HomePage")),
  },
  {
    path: "/student-create-new",
    component: React.lazy(() => import("../containers/CreatingPage")),
  },
  {
    path: "/student-update",
    component: React.lazy(() => import("../containers/UpdatingPage")),
  },
  {
    path: "/student-detail",
    component: React.lazy(() => import("../containers/DetailPage")),
  },
];
