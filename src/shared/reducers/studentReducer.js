import STUDENTS from "../actons/actionTypes";
import initialState from './initState';

export default function studentReducer(state = initialState.students, action) {
  switch(action.type) {
    case STUDENTS.FETCH_STUDENTS_SUCCESS: {
      return [...action.students];
    }
    default:
        return state;
  }
}
