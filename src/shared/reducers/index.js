import { combineReducers } from "redux";
import studentReducers from "./studentReducer";

const rootReducer = combineReducers({
    students: studentReducers,
});
  
export default rootReducer;