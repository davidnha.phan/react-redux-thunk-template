import STUDENTS from "./actionTypes";
import {getStudents} from '../../service/BaseApi';

const fetchStudents = () =>({type: STUDENTS.FETCH_STUDENTS})

const fetchStudentSuccess = (students) => ({
    type: STUDENTS.FETCH_STUDENTS_SUCCESS,
    students
})

const retrieveStudents = () => {
    return async(dispatch) => {
        dispatch(fetchStudents());
        const {data} = await getStudents();
        dispatch(fetchStudentSuccess(data));
    }
}

export {fetchStudentSuccess, fetchStudents, retrieveStudents}