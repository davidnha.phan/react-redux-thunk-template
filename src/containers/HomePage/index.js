import React, { useEffect } from 'react'
import { useNavigate } from "react-router-dom";
import { useSelector, useDispatch } from 'react-redux'

import {retrieveStudents} from '../../shared/actons/studentAction';
import Table from "../../components/Table";

const HomePage = () => {
  const navigate = useNavigate();
  const dispatch = useDispatch()

  const students = useSelector(state => state.students);

  useEffect(() => {
    dispatch(retrieveStudents())
  }, []);

  const handleShowPage = () => {
    navigate("/student-detail");
  };

  return (
    <div>
      <h1>List Student</h1>
      <Table handleShowPage={handleShowPage} students={students} />
    </div>
  );
};

export default HomePage;
