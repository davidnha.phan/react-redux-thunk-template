import styled from "styled-components";

import Button from "../Button";
import { BORDER_WHILTE, GREEN, WHITE } from "../../constants/colors";

const Table = styled.table`
  font-family: arial, sans-serif;
  border-collapse: collapse;
  width: 100%;
`;

const TH = styled.th`
  border: 1px solid ${BORDER_WHILTE};
  text-align: left;
  padding: 8px;
`;

const TD = styled.td`
  border: 1px solid ${BORDER_WHILTE};
  text-align: left;
  padding: 8px;
`;

const TR = styled.tr`
  &: nth-child(even) {
    background-color: ${BORDER_WHILTE};
  }
`;

const tableStudent = ({ students, handleShowPage }) => {
  return (
    <Table>
      <thead>
        <TR>
          <TH>ID.</TH>
          <TH>First Name</TH>
          <TH>Last Name</TH>
          <TH>Age</TH>
          <TH>Actions</TH>
        </TR>
      </thead>
      <tbody>
        {students.map((student) => {
          const { id, firstName, lastName, age } = student;
          return (
            <TR key={id}>
              <TD>{id}</TD>
              <TD>{firstName}</TD>
              <TD>{lastName}</TD>
              <TD>{age}</TD>
              <TD>
                <Button
                  data-testid={`show-button-${id}`}
                  onClick={handleShowPage}
                  backgroudColor={GREEN}
                  color={WHITE}
                >
                  Show
                </Button>
              </TD>
            </TR>
          );
        })}
      </tbody>
    </Table>
  );
};

export default tableStudent;
