import "@testing-library/jest-dom";
import { fireEvent, render } from "@testing-library/react";

import students from "./fakeData/students.json";
import Table from "../index";

test("render tables", () => {
  const { getByText } = render(<Table students={students} />);
  expect(getByText("ID.")).toBeInTheDocument();
  expect(getByText("First Name")).toBeInTheDocument();
  expect(getByText("Last Name")).toBeInTheDocument();
  expect(getByText("Age")).toBeInTheDocument();
  expect(getByText("Actions")).toBeInTheDocument();
});

const mockedUsedNavigate = jest.fn();

jest.mock("react-router-dom", () => ({
  ...jest.requireActual("react-router-dom"),
  useNavigate: () => mockedUsedNavigate("/"),
}));

test("handle click negative", () => {
  const { getByTestId } = render(
    <Table handleShowPage={mockedUsedNavigate} students={students} />
  );
  fireEvent.click(getByTestId("show-button-1"));
  expect(mockedUsedNavigate).toHaveBeenCalledTimes(1);
});
