import styled from "styled-components";
import { BUTTON_FONT_SIZE } from "../../constants/size";

const Button = styled.button`
  background-color: ${(props) => props.backgroudColor};
  border: none;
  color: ${(props) => props.color};
  padding: 10px 20px;
  text-align: center;
  text-decoration: none;
  display: inline-block;
  font-size: ${BUTTON_FONT_SIZE}px;
  margin: 4px 2px;
  cursor: pointer;
`;

export default Button;
