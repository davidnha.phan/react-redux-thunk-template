import { render } from "@testing-library/react";
import "@testing-library/jest-dom";

import Buttom from "../index";

test("renders a message", () => {
  const { getByText } = render(
    <Buttom backgroundColor="#f7f" color="#fff">
      Hello
    </Buttom>
  );
  expect(getByText("Hello")).toBeInTheDocument();
});
